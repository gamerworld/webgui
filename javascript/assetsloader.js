//Gw icons
var GWIcons = "https://gitlab.com/linuxbombay/gamerworld/assets/-/raw/main/images/icons";
//Gw Logo
var GWLogo = "https://gitlab.com/linuxbombay/gamerworld/assets/-/raw/main/images/logo";
//Gw BG
var GWBG = "https://gitlab.com/linuxbombay/gamerworld/assets/-/raw/main/images/bg";

//System UI

//UserIcon
  //var userIcon = GWIcons + "user.svg";
// Set the src attribute of the Usericon element to the constructed URL
  //document.getElementById('userIcon').src = userIcon;
//AboutIcon
  //var AboutIcon = GWIcons + "about.svg";
// Set the src attribute of the Usericon element to the constructed URL
  //document.getElementById('AboutIcon').src = AboutIcon;
//Logo
var GWLogoPath = GWLogo + "gwlogo3.png";
// Prepare the CSS rule
var elements = document.getElementsByClassName('.GWLogo');
for (var i = 0; i < elements.length; i++) {
    elements[i].style.backgroundImage = "url('" + GWLogoPath + "')";
}

//GamerWorld Flash Archive URL
var GWflashurl = "https://gw-gamearchive.netlify.app/flash";
//GamerWorld HTML Archive URL
var GWhtmlurl = "https://gw-gamearchive.netlify.app/html5";
